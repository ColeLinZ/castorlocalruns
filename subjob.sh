#!/bin/bash

SHDIR=/afs/cern.ch/work/c/clindsey/castorLocalRuns/CMSSW_10_3_0/src/CalibCalorimetry/CastorCalib/castorlocalruns/shFiles

export LSB_JOB_REPORT_MAIL=N

while read -r line || [[ -n "$line" ]]; do

	NAME="$(echo "$line" | tr -d .root | tr -d ./ | tr -d USC_ )"
	SHF=${SHDIR}/sh_${NAME}.sh
	echo  "submitting ${SHF}"
	bsub  -q 8nh ${SHF}

done < "${1}"
