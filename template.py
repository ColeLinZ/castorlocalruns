import FWCore.ParameterSet.Config as cms

process = cms.Process("PEDS")
process.load("CondCore.DBCommon.CondDBSetup_cfi")

process.source = cms.Source("HcalTBSource",
    streams = cms.untracked.vstring('HCAL_Trigger', 
        'HCAL_DCC690','HCAL_DCC691','HCAL_DCC692', 
),
    
    
    fileNames = cms.untracked.vstring(

"switcherParty" 

# 2.11.2018:
# USC_325686.root
# USC_325694.root
# at 3.8T
# USC_325791.root

       )   
    )

    
process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(-1)
    #input = cms.untracked.int32(-1)
)

#Merijn try to add some output since run over 1M evts.
process.load('FWCore.MessageService.MessageLogger_cfi')
process.MessageLogger.cout.FwkReport = cms.untracked.PSet(
    reportEvery = cms.untracked.int32(1)
    )

process.castorDigis = cms.EDProducer("CastorRawToDigi",
    # Optional filter to remove any digi with "data valid" off, "error" on, 
    # or capids not rotating
    FilterDataQuality = cms.bool(True),
    # Number of the first CASTOR FED.  If this is not specified, the
    # default from FEDNumbering is used.
    CastorFirstFED = cms.int32(690),
    ZDCFirstFED = cms.int32(693),                         
    # FED numbers to unpack.  If this is not specified, all FEDs from
    # FEDNumbering will be unpacked.
    FEDs = cms.untracked.vint32( 690, 691, 692, 693, 722),
    # Do not complain about missing FEDs
    ExceptionEmptyData = cms.untracked.bool(False),
    # Do not complain about missing FEDs
    ComplainEmptyData = cms.untracked.bool(False),
    # At most ten samples can be put into a digi, if there are more
    # than ten, firstSample and lastSample select which samples
    # will be copied to the digi
    firstSample = cms.int32(0),
    lastSample = cms.int32(9),
    # castor technical trigger processor
    UnpackTTP = cms.bool(True),
    # report errors
    silent = cms.untracked.bool(False),
    #
    InputLabel = cms.InputTag("source"),
    CastorCtdc = cms.bool(False),
    UseNominalOrbitMessageTime = cms.bool(True),
    ExpectedOrbitMessageTime = cms.int32(-1),
    UnpackZDC = cms.bool(False)
)

process.out = cms.OutputModule("PoolOutputModule",
    fileName = cms.untracked.string('tricksy_unpacked.root')
)
process.dumpRaw = cms.EDAnalyzer( "DumpFEDRawDataProduct",

   feds = cms.untracked.vint32( 690,691,692,693 ),
   dumpPayload = cms.untracked.bool( True )
)

process.m = cms.EDAnalyzer("HcalDigiDump")

process.dump = cms.EDAnalyzer('HcalTBObjectDump',
                              hcalTBTriggerDataTag = cms.InputTag('tbunpack'),
                              hcalTBRunDataTag = cms.InputTag('tbunpack'),
                              hcalTBEventPositionTag = cms.InputTag('tbunpack'),
                              hcalTBTimingTag = cms.InputTag('tbunpack')
)

process.dumpECA = cms.EDAnalyzer("EventContentAnalyzer")

process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
from Configuration.AlCa.GlobalTag import GlobalTag
from CondCore.CondDB.CondDB_cfi import *
process.GlobalTag = GlobalTag(process.GlobalTag, '101X_dataRun2_Prompt_v11', '')




process.castorpedestalsanalysis = cms.EDAnalyzer("CastorPedestalsAnalysis",
    hiSaveFlag  = cms.untracked.bool( False ),
    verboseflag = cms.untracked.bool( True ),
    firstTS = cms.untracked.int32(0),
    #lastTS = cms.untracked.int32(9),#Merijn 2018 9 8 originally it was 9. Also set in CASTOR raw to digi above..
    lastTS = cms.untracked.int32(10),#Merijn 2018 9 8 originally it was 9. Also set in CASTOR raw to digi above..
    castorDigiCollectionTag = cms.InputTag('castorDigis')
)


#process.MessageLogger.destinations = ['detailedInfo.txt']

#process.p = cms.Path(process.dumpRaw*process.castorDigis*process.dump*process.m*process.dumpECA)
process.p = cms.Path(process.castorDigis*process.castorpedestalsanalysis)
#process.p = cms.Path(process.castorDigis)
process.ep = cms.EndPath(process.out)

