#!/bin/bash
export XRD_NETWORKSTACK=IPv4
export LSB_JOB_REPORT_MAIL=N
export X509_USER_PROXY=/afs/cern.ch/user/c/clindsey/x509up_u93196

cd /afs/cern.ch/work/c/clindsey/castorLocalRuns/CMSSW_10_3_0/src
 
eval `scramv1 runtime -sh`
 
cd /afs/cern.ch/work/c/clindsey/castorLocalRuns/CMSSW_10_3_0/src/CalibCalorimetry/CastorCalib/test

cmsRun wonkySwitch
