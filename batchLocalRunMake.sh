#!/bin/bash

DIR=/afs/cern.ch/work/c/clindsey/castorLocalRuns/CMSSW_10_3_0/src/CalibCalorimetry/CastorCalib/castorlocalruns
PYDIR=${DIR}/pyFiles
SHDIR=${DIR}/shFiles
INDIR=/afs/cern.ch/work/c/clindsey/public/castorLocals
# DIR: Wherever this file is
# PYDIR/SHDIR: Don't update
# INDIR: Wherever the local run root files are stored

while read -r line || [[ -n "$line" ]]; do

        NAME="$(echo "$line" | tr -d .root | tr -d ./ | tr -d USC_ )"

	SHF=${SHDIR}/sh_${NAME}.sh
	PYF=${PYDIR}/py_${NAME}.py
	INF=${INDIR}/USC_${NAME}.root

	cp template.py ${PYF}

	sed -i "s|switcherParty|file:${INF}|g" ${PYF}
	sed -i "s|tricksy|USC_${NAME}|g" ${PYF}

	cp template.sh ${SHF}

	sed -i "s|wonkySwitch|${PYF}|g" ${SHF}

	echo "File:${SHF} was created"

done < "${1}"
